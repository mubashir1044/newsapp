import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import {Card} from 'react-native-elements'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as MainActions from '../redux/actions/index'
import HomeCard from '../components/homeCard'


 class Home extends Component {
  constructor(){
    super();
    this.state={
      news:[],
      favourites:[]
    }

  }
  async componentWillMount(){
    try{
      let ids = await AsyncStorage.getItem('favourite');
      console.log('ids are:',ids)
      if(ids){
        let idsTemp = JSON.parse(ids)
        console.log('parse data is: ',idsTemp)
        this.props.setFavourites(idsTemp)
        this.setState({favourites:idsTemp})
      }
    }
    catch(err){
      console.log('error in getting data is: ',err)
    }
    
  }
   componentDidMount(){
this.props.fetchingData();
this.props.fetchNewsData('Home');
this.props.recievedData()


  }
  handleFav = (id,fav)=>{
    console.log('previous favourites are: ',fav)
    if(fav){
      let temp = this.state.favourites;
      for(let i=0;i<this.state.favourites.length;i++){
        if(id===this.state.favourites[i]){
          console.log('before splice',temp)
          temp.splice(i,1);
          console.log('after splice',temp)
          this.setState({favourites:temp});
          AsyncStorage.setItem('favourite',JSON.stringify(temp));
          this.props.setFavourites(temp)
          break;
        }
      }
    }
    else{
     let temp = this.state.favourites;
     temp.push(id);
     this.setState({favourites:temp});
     this.props.setFavourites(temp)
     AsyncStorage.setItem('favourite',JSON.stringify(temp))
    }
   
  
  }
 

  
  render() {
    return (
      <View style={{flex:1,backgroundColor:'#fff'}}>
       {this.props.fetching?
       <View style={{flex:1,justifyContent:'center',alignItems:'center'}} >
        <ActivityIndicator size="large" color="#00ff00" />
       </View>
       :
       !this.props.fetching&&this.props.newsData.data.length===0?
       <View style={{flex:1,justifyContent:'center'}}>
       <Text style={{textAlign:'center',fontSize:18}}>No Data in {this.props.newsData.screen}</Text>
       </View>
       :
       <View style={{flex:1}}>
         <FlatList
  data={this.props.newsData.data}
  numColumns={1}
  renderItem={({item,index}) => (
<HomeCard screen={this.props.newsData.screen} data={item} navigation={this.props.navigation}  handleFav={this.handleFav} />

 )}
/>
       </View>
      }
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
function mapStateToProps(state){
  return{
    newsData:state.newsData,
    fetching:state.fetching,
  };
}
function mapDispatchToProps(dispatch){
  return bindActionCreators(MainActions,dispatch);
  }
export default connect(mapStateToProps,mapDispatchToProps)(Home);
