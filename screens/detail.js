import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image
} from 'react-native';


export default class Detail extends Component {
  constructor(){
    super();
    this.state={
      data:{}
    }
  }
  componentWillMount(){
    this.setState({data:this.props.navigation.state.params.data})
  }
  
  render() {
    return (
      <ScrollView style={{flex:1,backgroundColor:'white'}}>
      <View style={{flex:1}}>

<Image source={{uri:this.state.data.picUrl}} style={{height:200}}/>
      </View>
      <View style={{flex:2,margin:'5%'}}>
  <Text style={{marginBottom:15,fontSize:18,fontWeight:'bold',color:'black'}}>
  {this.state.data.title}
  </Text>
  <Text style={{marginBottom:15,fontSize:15,color:'black'}}>
  {this.state.data.description}
  </Text>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

});
