import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import {Card, Icon} from 'react-native-elements'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as MainActions from '../redux/actions/index'

 class HomeCard extends Component {
  constructor(){
    super();
    this.state={
        fav:false,
    }
  
    }

   componentDidMount(){
     setTimeout(()=>{
       this.setState({fav:this.props.favourites.filter(id => id==this.props.data.id).length>0?true:false})
     },1000)
   }
  

  handleFavourite = ()=>{

     this.props.handleFav(this.props.data.id,this.state.fav);
     this.setState({fav:!this.state.fav})
  }

  

  
  render() {
   
    return (
     <View>
<TouchableOpacity style={{position:'relative'}} onPress={()=>{this.props.navigation.navigate('Detail',{data:this.props.data})}}>
      <Card
    
    
    image={{uri:this.props.data.picUrl}}
    // title='HELLO WORLD'
    >
    <Text style={{color:'black',fontSize:18,fontWeight:'bold'}}>{this.props.data.title}</Text>
    
    <Text style={{marginBottom: 10,fontSize:14}}>

      {this.props.data.description.length<80?this.props.data.description:this.props.data.description.substr(0,80).concat('...')}
    </Text>
   
  </Card>
  </TouchableOpacity>
  {this.props.screen!=='Favourite'?
  <TouchableOpacity onPress={()=>{this.handleFavourite()}} style={{position:'absolute',top:20,right:20,zIndex:5,height:50,width:50}}>
      <Icon type="materialicon" name={this.props.favourites.filter(id => id==this.props.data.id).length>0?`favorite`:`favorite-border`} color={this.props.favourites.filter(id => id==this.props.data.id).length>0?'#ff0000':'#000'} size={30} />
  </TouchableOpacity>
  :null}
  </View>

 
    );
  }
}

const styles = StyleSheet.create({

});

function mapStateToProps(state){
  return{
    favourites:state.favourites
  };
}
function mapDispatchToProps(dispatch){
  return bindActionCreators(MainActions,dispatch);
  }
export default connect(mapStateToProps,mapDispatchToProps)(HomeCard);


