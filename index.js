import { AppRegistry } from 'react-native';
import App from './App';
import React, { Component } from 'react';

import {Provider} from 'react-redux'
import configureStore from './redux/store/configureStore'
const store = configureStore();

const rnRedux = ()=>(
<Provider store = {store}>
 <App/>
 {/* <Text>asi</Text> */}
  </Provider>
)

AppRegistry.registerComponent('newsapp', () => rnRedux);
