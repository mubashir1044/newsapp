import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import MainNavigation from './routes/mainNavigation'


export default class App extends Component {
  constructor(){
    super();
    console.disableYellowBox=true
  }
  render() {
    return (
     
     <MainNavigation/>
     
    );
  }
}

const styles = StyleSheet.create({

});
