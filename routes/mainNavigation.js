import React from 'react';
import {Platform} from 'react-native'
import {DrawerNavigator} from 'react-navigation'
import Home from '../screens/home'
import DrawerContent from './drawerContent'
import ScreenNavigation from '../routes/screenNavigation'


const DrawerRoutes =({
  Home: { 
    screen:ScreenNavigation
  },
  Trending: { 
    screen:ScreenNavigation
  },
  Favourite: { 
    screen:ScreenNavigation
  },
})

const  MainNavigation = DrawerNavigator(DrawerRoutes, 
  {
    initialRouteName:'Home',
    contentComponent:({navigation})=> <DrawerContent navigation={navigation} routes={DrawerRoutes} />, //you dont need the routes props, but just in case you wanted to use those instead for the navigation item creation you could
    mode: Platform.OS === 'ios' ? 'modal' : 'card',
  }
) 



export default MainNavigation;