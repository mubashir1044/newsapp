// import React from 'react';
import React, {
    Component
} from 'react';

import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    TouchableHighlight
} from 'react-native';
import {StackNavigator} from 'react-navigation'
import Home from '../screens/home'
import Detail from '../screens/detail'
import {Header,Icon} from 'react-native-elements'
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as MainActions from '../redux/actions/index'
const App = StackNavigator({

    Home: { screen: Home },
    Detail: { screen: Detail },
    
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
   
   }
  );
class ScreenNavigation extends Component {
    constructor(props) {
        super(props);
        this.state={
            screen:'Home'
        }
       
    }
   
    render() {
        return (
            <View style={{flex:1}}>
            <View style={{flex:1}}>
            <Header
                    outerContainerStyles={{ flex: 1, borderBottomWidth: 0, backgroundColor: '#16a085' }}
                    leftComponent={
                        <TouchableOpacity onPress={()=>{this.props.navigation.toggleDrawer()}} >      
                            <Icon type="entypo" name="menu" color="#fff"/>                 
                   </TouchableOpacity>
                    }

                    centerComponent={{ text: this.props.newsData.screen, style: { color: '#fff', textAlign: 'left',fontSize:18 } }}
                    // rightComponent={{ icon: 'search', color: '#fff'}}
                />
            </View>
            <View style={{flex:10}}>
            <App screenProps={{screen:this.state.screen}}/>
            </View>
              
                </View>
        )
    }
}

function mapStateToProps(state){
    return{
      newsData:state.newsData
    };
  }
  function mapDispatchToProps(dispatch){
    return bindActionCreators(MainActions,dispatch);
    }
  export default connect(mapStateToProps,mapDispatchToProps)(ScreenNavigation);
