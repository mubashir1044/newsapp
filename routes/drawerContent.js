// import React from 'react';
import React, {
    Component
} from 'react';

import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';
import {connect} from 'react-redux';
import {Icon} from 'react-native-elements'
import { bindActionCreators } from 'redux';
import * as MainActions from '../redux/actions/index'


class DrawerContent extends Component {
    constructor(props) {
        super(props);
        this.handleFavourite = this.handleFavourite.bind(this)
       
    }
    handlePress =async (value)=>{
        console.log('called',value);
        if(value==='Favourite'){
            this.handleFavourite(value)
        }
        else{
        this.props.fetchingData();
      this.props.fetchNewsData(value,[])
           
        
        this.props.recievedData()
        this.props.navigation.toggleDrawer()
        }
    }
    async handleFavourite (value){
        try{
            let ids = await AsyncStorage.getItem('favourite');
            this.props.fetchingData();
            if(ids){
                let tempIds = JSON.parse(ids);
                
      this.props.fetchNewsData(value,tempIds)
           
        
       
            }
            else{
              
      this.props.fetchNewsData(value,[])
           
      
            }
              
        this.props.recievedData()
        this.props.navigation.toggleDrawer()
           
          }
          catch(err){
            console.log('error in getting data is: ',err)
          }
          
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 3, backgroundColor: '#16a085', justifyContent: 'center'}}>
                  
                    <Text style={{color:'white',fontSize:20,fontWeight:'bold',textAlign:'center'}}>News App</Text>
                </View>
                <View style={{ flex: 22, marginTop: 10 }}>
                    <TouchableOpacity style={{paddingTop:20  }} onPress={() => {
                        this.handlePress('Home')
                        
                    }
                    }>

                        <View style={{ flexDirection: 'row' }}>
                           <View style={{flex:1}}>
                                <Icon name="home" type="entypo" color={this.props.newsData.screen==='Home'?'green':'black'}/>
                           </View>
                            <Text style={{ flex: 3, fontSize: 16, color: this.props.newsData.screen==='Home'?'green':'black' }}>Home</Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity style={{ paddingTop:20 }} onPress={() => {
                        this.handlePress('Trending')
                        
                    }
                    }>

                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                            <Icon name="chart" type="evilicon" color={this.props.newsData.screen==='Trending'?'green':'black'}/>
                            </View>
                            <Text style={{ flex: 3, fontSize: 16, color: this.props.newsData.screen==='Trending'?'green':'black' }}>Trending</Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity style={{ paddingTop:20  }} onPress={() => {
                        this.handlePress('Favourite')
                    }
                    }>

                        <View style={{ flexDirection: 'row' }}>
                          <View style={{flex:1}}>
                          <Icon name="favorite" type="materialicon" color={this.props.newsData.screen==='Favourite'?'green':'black'}/>
                          </View>
                              
                            <Text style={{ flex: 3, fontSize: 16, color: this.props.newsData.screen==='Favourite'?'green':'black' }}>Favourite</Text>
                        </View>

                    </TouchableOpacity>
                </View>

            </View>)
    }
}

function mapStateToProps(state){
    return{
      newsData:state.newsData
    };
  }
  function mapDispatchToProps(dispatch){
    return bindActionCreators(MainActions,dispatch);
    }
  export default connect(mapStateToProps,mapDispatchToProps)(DrawerContent);
  