// Set up your root reducer here...
import { combineReducers } from 'redux';
// import {routerReducer} from 'react-router-redux';
import NewsReducer from './newsReducer'
import LoadingReducer from './loadingReducer'
import FavouriteReducer from './favouriteReducer'
const rootReducer = combineReducers (
    {
        newsData:NewsReducer,
        fetching:LoadingReducer,
        favourites:FavouriteReducer
      
    }
)
export default rootReducer;