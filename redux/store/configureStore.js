import {createStore, compose, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';

export default function configureStore() {
let store = createStore(rootReducer,applyMiddleware(thunkMiddleware))
return store;

}